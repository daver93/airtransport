This project was implemented by using Visual Studio 2022 Community Edition.

It uses ASP.NET Core 6 and Entity Framework.
This project follows the Domain Driven Design (DDD) and Dependency Injection.

The code contains seed data, so it will be ready for testing.

When you will run the project, if the database does not exist already in your system it should be created automatically [this is defined into the Program.cs file of the AirTransport.Web] project.

The AirTransport.Web should be the startup project.