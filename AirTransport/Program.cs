using AirTransport.Application.Services;
using AirTransport.Domain.IRepositories;
using AirTransport.Domain.IServices;
using AirTransport.Infrastructure;
using AirTransport.Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using VendingMachineDemo.Data;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddControllers();
builder.Services.AddSwaggerGen();
builder.Services.AddDbContext<AirTransportDbContext>(options =>
                options.UseSqlServer(builder.Configuration.GetConnectionString("AirTransportContext")));

// Dependency Injection
builder.Services.AddScoped<IAirportService, AirportService>();
builder.Services.AddScoped<IGeographyLevel1Service, GeographyLevel1Service>();
builder.Services.AddScoped<IRouteService, RouteService>();

builder.Services.AddScoped<IAirportRepository, AirportRepository>();
builder.Services.AddScoped<IGeographyLevel1Repository, GeographyLevel1Repository>();
builder.Services.AddScoped<IRouteRepository, RouteRepository>();

var app = builder.Build();

// Ensure the database is created and seed the data
using (var scope = app.Services.CreateScope())
{
    var services = scope.ServiceProvider;
    var context = services.GetRequiredService<AirTransportDbContext>();
    context.Database.EnsureCreated();
    SeedData.InitializeDatabase(context);
}

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.MapControllers(); // Add this line to enable routing for controllers

app.Run();