﻿using AirTransport.Application.DTOs.Routes;
using AirTransport.Domain.Enums;
using AirTransport.Domain.IServices;
using AirTransport.Domain.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace AirTransport.Web.Controllers
{
    [ApiController]
    [Route("routes")]
    public class RouteController : ControllerBase
    {
        private readonly IRouteService _routeService;
        private readonly IAirportService _airportService;

        public RouteController(IRouteService routeService, IAirportService airportService)
        {
            _routeService = routeService;
            _airportService = airportService;
        }

        [HttpGet]
        public async Task<ActionResult<GetRoutesResponse>> GetRoutes()
        {
            try
            {
                var routes = await _routeService.GetRoutes();

                if (routes.IsNullOrEmpty())
                {
                    return NotFound("No countries found.");
                }

                var response = new GetRoutesResponse();

                foreach (var route in routes)
                {
                    response.Routes.Add(new RouteDTO
                    {
                        Id = route.RouteID,
                        DepartureAirportID = route.DepartureAirportID,
                        ArrivalAirportID = route.ArrivalAirportID
                    });
                }

                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest("Something went wrong.");
            }
        }

        [HttpPost]
        public async Task<ActionResult> CreateRoute(BaseRouteRequest request)
        {
            if (request == null)
            {
                return BadRequest("Invalid request.");
            }
            else if (request.CreateRouteRequest != null)
            {
                return await CreateRoute(request.CreateRouteRequest);
            }
            else if (request.CreateGroupRouteRequest != null)
            {
                return await CreateRouteWithGroups(request.CreateGroupRouteRequest);
            }

            return BadRequest("Invalid request.");
        }

        private async Task<ActionResult> CreateRoute(CreateRouteRequest request)
        {
            try
            {
                if (request.DepartureAirportID == 0)
                {
                    return BadRequest($"Invalid {nameof(request.DepartureAirportID)}.");
                }
                else if (request.ArrivalAirportID == 0)
                {
                    return BadRequest($"Invalid {nameof(request.ArrivalAirportID)}.");
                }

                var existingRoute = await _routeService.GetRouteByDepartureAndArrival(request.DepartureAirportID, request.ArrivalAirportID);

                if (existingRoute != null)
                {
                    return BadRequest("Route already exists.");
                }

                var departureAirport = await _airportService.GetAirportById(request.DepartureAirportID);

                if (departureAirport == null)
                {
                    return BadRequest($"Departure airport not found.");
                }
                else if (departureAirport.Type == AirportType.ArrivalOnly.ToString())
                {
                    return BadRequest($"Invalid route.");
                }

                var arrivalAirport = await _airportService.GetAirportById(request.ArrivalAirportID);

                if (arrivalAirport == null)
                {
                    return BadRequest($"Arrival airport not found.");
                }
                else if (arrivalAirport.Type == AirportType.DepartureOnly.ToString())
                {
                    return BadRequest($"Invalid route.");
                }

                var route = new Domain.Models.Route
                {
                    DepartureAirportID = departureAirport.AirportID,
                    ArrivalAirportID = arrivalAirport.AirportID
                };

                var created = await _routeService.CreateRoute(route);

                if (created)
                {
                    return Ok("Route succesfully created.");
                }

                return BadRequest("Route has not been created.");
            }
            catch (Exception ex)
            {
                return BadRequest("Something went wrong.");
            }
        }

        private async Task<ActionResult> CreateRouteWithGroups(CreateGroupRouteRequest request)
        {
            try
            {
                if (request.DepartureAirportGroupID == 0)
                {
                    return BadRequest($"Invalid {nameof(request.DepartureAirportGroupID)}.");
                }
                else if (request.ArrivalAirportGroupID == 0)
                {
                    return BadRequest($"Invalid {nameof(request.ArrivalAirportGroupID)}.");
                }

                // Retrieve the airports associated with the airport group IDs
                var departureAirports = await _airportService.GetAirportsByGroupId(request.DepartureAirportGroupID);
                var arrivalAirports = await _airportService.GetAirportsByGroupId(request.ArrivalAirportGroupID);

                if (departureAirports.Count == 0)
                {
                    return BadRequest($"Departure airports not found.");
                }
                else if (departureAirports.Any(a => a.Type == AirportType.ArrivalOnly.ToString()))
                {
                    return BadRequest($"There is one or more invalid route.");
                }

                if (arrivalAirports.Count == 0)
                {
                    return BadRequest($"Arrival airports not found.");
                }
                else if (arrivalAirports.Any(a => a.Type == AirportType.DepartureOnly.ToString()))
                {
                    return BadRequest($"There is one or more invalid route.");
                }

                List<Domain.Models.Route> routes = new List<Domain.Models.Route>();

                // Create routes for each combination of departure and arrival airports
                foreach (var departureAirport in departureAirports)
                {
                    foreach (var arrivalAirport in arrivalAirports)
                    {
                        var existingRoute = await _routeService.GetRouteByDepartureAndArrival(departureAirport.AirportID, arrivalAirport.AirportID);

                        if (existingRoute != null)
                        {
                            return BadRequest("Route already exists.");
                        }

                        var route = new Domain.Models.Route
                        {
                            DepartureAirportID = departureAirport.AirportID,
                            ArrivalAirportID = arrivalAirport.AirportID
                        };

                        routes.Add(route);
                    }
                }

                if (routes.Count > 0)
                {
                    int countFailures = 0;

                    // Try and save each route into the database
                    foreach (var route in routes)
                    {
                        var created = await _routeService.CreateRoute(route);

                        if (!created)
                        {
                            countFailures++;
                        }
                    }

                    if (countFailures > 0)
                    {
                        int countSuccess = routes.Count - countFailures;
                        return Ok($"{countSuccess} have been created and {countFailures} routes failed to be created.");
                    }
                    else
                    {
                        return Ok($"Routes successfully created.");
                    }
                }

                return BadRequest("No routes have been created.");
            }
            catch (Exception ex)
            {
                return BadRequest("Something went wrong.");
            }
        }
    }
}
