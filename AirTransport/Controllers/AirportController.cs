﻿using AirTransport.Application.DTOs.Airports;
using AirTransport.Domain.IServices;
using AirTransport.Domain.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace AirTransport.Controllers
{
    [ApiController]
    [Route("airports")]
    public class AirportController : ControllerBase
    {
        private readonly IAirportService _airportService;

        public AirportController(IAirportService airportService)
        {
            _airportService = airportService;
        }

        [HttpGet]
        public async Task<ActionResult<GetAirportsResponse>> GetAirports()
        {
            try
            {
                var airports = await _airportService.GetAirports();

                if (airports.IsNullOrEmpty())
                {
                    return NotFound("No airports found.");
                }

                var response = new GetAirportsResponse();

                foreach (var airport in airports)
                {
                    response.Airports.Add(new AirportDTO
                    {
                        Id = airport.AirportID,
                        IATACode = airport.IATACode,
                        Type = airport.Type,
                        GeographyLevel1ID = airport.GeographyLevel1ID
                    });
                }

                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest("Something went wrong.");
            }
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<GetAirportResponse>> GetAirport(int id)
        {
            var airport = await _airportService.GetAirportById(id);

            if (airport == null)
            {
                return NotFound("Airport not found.");
            }

            var response = new GetAirportResponse
            {
                Airport = new AirportDTO
                {
                    Id = airport.AirportID,
                    IATACode = airport.IATACode,
                    Type = airport.Type,
                    GeographyLevel1ID = airport.GeographyLevel1ID
                }
            };

            return Ok(response);
        }
    }
}
