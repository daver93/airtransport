﻿using AirTransport.Application.DTOs.Countries;
using AirTransport.Domain.IServices;
using AirTransport.Domain.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace AirTransport.Web.Controllers
{
    [ApiController]
    [Route("countries")]
    public class CountryController : ControllerBase
    {
        private readonly IGeographyLevel1Service _countryService;

        public CountryController(IGeographyLevel1Service airportService)
        {
            _countryService = airportService;
        }

        [HttpGet]
        public async Task<ActionResult<GetCountriesResponse>> GetCountries()
        {
            try
            {
                var countries = await _countryService.GetCountries();

                if (countries.IsNullOrEmpty())
                {
                    return NotFound("No countries found.");
                }

                var response = new GetCountriesResponse();

                foreach (var country in countries)
                {
                    response.Countries.Add(new CountryDTO
                    {
                        Id = country.GeographyLevel1ID,
                        Name = country.Name
                    });
                }

                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest("Something went wrong.");
            }
        }

        [HttpPost]
        public async Task<ActionResult> CreateCountry(CreateCountryRequest request)
        {
            try
            {
                var countryName = request.Name.Trim();

                if (countryName == null)
                {
                    return BadRequest($"{nameof(request.Name)} is not valid");
                }

                var country = new GeographyLevel1 { Name = countryName };

                var created = await _countryService.CreateCountry(country);

                if (created)
                {
                    return Ok("Country succesfully created.");
                }

                return BadRequest("Country has not been created.");
            }
            catch (Exception ex)
            {
                return BadRequest("Something went wrong.");
            }
        }

        [HttpDelete]
        public async Task<ActionResult> DeleteCountry([FromQuery(Name = "ID")] int id)
        {
            try
            {
                var deleted = await _countryService.DeleteCountry(id);

                if (deleted)
                {
                    return Ok("Country succesfully deleted.");
                }

                return BadRequest("Country did not delete.");
            }
            catch (Exception ex)
            {
                return BadRequest("Something went wrong.");
            }
        }
    }
}
