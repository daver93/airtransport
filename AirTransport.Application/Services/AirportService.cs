﻿using AirTransport.Domain.IRepositories;
using AirTransport.Domain.IServices;
using AirTransport.Domain.Models;

namespace AirTransport.Application.Services
{
    public class AirportService : IAirportService
    {
        private readonly IAirportRepository _airportRepository;

        public AirportService(IAirportRepository airportRepository)
        {
            _airportRepository = airportRepository;
        }

        public async Task<IList<Airport>> GetAirports()
        {
            return await _airportRepository.GetAirports();
        }

        public async Task<Airport> GetAirportById(int id)
        {
            return await _airportRepository.GetAirport(id);
        }

        public async Task<IList<Airport>> GetAirportsByGroupId(int airportGroupID)
        {
            return await _airportRepository.GetAirportsByGroupId(airportGroupID);
        }

    }
}
