﻿using AirTransport.Domain.IRepositories;
using AirTransport.Domain.IServices;
using AirTransport.Domain.Models;

namespace AirTransport.Application.Services
{
    public class GeographyLevel1Service : IGeographyLevel1Service
    {
        private readonly IGeographyLevel1Repository _countryRepository;

        public GeographyLevel1Service(IGeographyLevel1Repository countryRepository)
        {
            _countryRepository = countryRepository;
        }

        public async Task<IList<GeographyLevel1>> GetCountries()
        {
            return await _countryRepository.GetCountries();
        }

        public async Task<bool> CreateCountry(GeographyLevel1 country)
        {
            return await _countryRepository.CreateCountry(country);
        }

        public async Task<bool> DeleteCountry(int id)
        {
            return await _countryRepository.DeleteCountry(id);
        }
    }
}
