﻿using AirTransport.Domain.IRepositories;
using AirTransport.Domain.IServices;
using AirTransport.Domain.Models;

namespace AirTransport.Application.Services
{
    public class RouteService : IRouteService
    {
        private readonly IRouteRepository _routeRepository;

        public RouteService(IRouteRepository routeRepository)
        {
            _routeRepository = routeRepository;
        }

        public async Task<IList<Route>> GetRoutes()
        {
            return await _routeRepository.GetRoutes();
        }

        public async Task<Route?> GetRouteByDepartureAndArrival(int departureAirportID, int arrivalAirportID)
        {
            return await _routeRepository.GetRouteByDepartureAndArrival(departureAirportID, arrivalAirportID);
        }

        public async Task<bool> CreateRoute(Route route)
        {
            return await _routeRepository.CreateRoute(route);
        }
    }
}
