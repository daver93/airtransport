﻿namespace AirTransport.Application.DTOs.Routes
{
    public class BaseRouteRequest
    {
        public CreateRouteRequest? CreateRouteRequest { get; set; }
        
        public CreateGroupRouteRequest? CreateGroupRouteRequest { get; set; }
    }

    public class CreateRouteRequest
    {
        public int DepartureAirportID { get; set; }

        public int ArrivalAirportID { get; set; }
    }

    public class CreateGroupRouteRequest
    {
        public int DepartureAirportGroupID { get; set; }

        public int ArrivalAirportGroupID { get; set; }
    }
}
