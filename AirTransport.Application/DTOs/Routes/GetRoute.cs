﻿namespace AirTransport.Application.DTOs.Routes
{
    public class GetRoutesResponse
    {
        public GetRoutesResponse()
        {
            Routes = new List<RouteDTO>();
        }

        public List<RouteDTO> Routes { get; set; }
    }

    public class RouteDTO
    {
        public int Id { get; set; }

        public int DepartureAirportID { get; set; }

        public int ArrivalAirportID { get; set; }
    }
}
