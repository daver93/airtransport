﻿namespace AirTransport.Application.DTOs.Airports
{
    public class GetAirportsResponse
    {
        public GetAirportsResponse()
        {
            Airports = new List<AirportDTO>();
        }

        public List<AirportDTO> Airports { get; set; }
    }

    public class GetAirportResponse
    {
        public AirportDTO Airport { get; set; }
    }

    public class AirportDTO
    {
        public int Id { get; set; }

        public string IATACode { get; set; }

        public int GeographyLevel1ID { get; set; }

        public string Type { get; set; }
    }
}
