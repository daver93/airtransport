﻿namespace AirTransport.Application.DTOs.Countries
{
    public class GetCountriesResponse
    {
        public GetCountriesResponse()
        { 
            Countries = new List<CountryDTO>();
        }

        public List<CountryDTO> Countries { get; set; }
    }

    public class CountryDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
