﻿namespace AirTransport.Application.DTOs.Countries
{
    public class CreateCountryRequest
    {
        public string Name { get; set; }
    }
}
