﻿using AirTransport.Domain.Models;

namespace AirTransport.Domain.IServices
{
    public interface IRouteService
    {
        Task<IList<Route>> GetRoutes();

        Task<Route?> GetRouteByDepartureAndArrival(int departureAirportID, int arrivalAirportID);

        Task<bool> CreateRoute(Route route);
    }
}
