﻿using AirTransport.Domain.Models;

namespace AirTransport.Domain.IServices
{
    public interface IGeographyLevel1Service
    {
        Task<IList<GeographyLevel1>> GetCountries();

        Task<bool> CreateCountry(GeographyLevel1 country);

        Task<bool> DeleteCountry(int id);
    }
}
