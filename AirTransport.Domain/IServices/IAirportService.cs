﻿using AirTransport.Domain.Models;

namespace AirTransport.Domain.IServices
{
    public interface IAirportService
    {
        Task<IList<Airport>> GetAirports();

        Task<Airport> GetAirportById(int id);

        Task<IList<Airport>> GetAirportsByGroupId(int airportGroupID);
    }
}
