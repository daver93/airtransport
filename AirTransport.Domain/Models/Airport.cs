﻿namespace AirTransport.Domain.Models
{
    public class Airport
    {
        public int AirportID { get; set; }

        public string IATACode { get; set; } = string.Empty;

        public int GeographyLevel1ID { get; set; }

        public string Type { get; set; }

        public GeographyLevel1 GeographyLevel1 { get; set; } // foreign key to GeographyLevel1

        public ICollection<AirportGroup> AirportGroups { get; set; }
    }
}
