﻿namespace AirTransport.Domain.Models
{
    public class AirportGroup
    {
        public AirportGroup()
        { 
            Airports = new List<Airport>();
        }

        public int AirportGroupID { get; set; }

        public string Name { get; set; }

        public ICollection<Airport> Airports { get; set; }
    }
}
