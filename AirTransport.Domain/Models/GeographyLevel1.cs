﻿using System.ComponentModel.DataAnnotations;

namespace AirTransport.Domain.Models
{
    public class GeographyLevel1
    {
        public int GeographyLevel1ID { get; set; }

        public string Name { get; set; } = string.Empty;
    }
}
