﻿namespace AirTransport.Domain.Models
{
    public class Route
    {
        public int RouteID { get; set; }

        public int DepartureAirportID { get; set; }

        public int ArrivalAirportID { get; set; }

        public Airport DepartureAirport { get; set; }

        public Airport ArrivalAirport { get; set; }
    }
}
