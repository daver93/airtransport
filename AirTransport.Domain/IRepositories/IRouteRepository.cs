﻿using AirTransport.Domain.Models;

namespace AirTransport.Domain.IRepositories
{
    public interface IRouteRepository
    {
        Task<IList<Route>> GetRoutes();

        Task<Route?> GetRouteByDepartureAndArrival(int departureAirportID, int arrivalAirportID);

        Task<bool> CreateRoute(Route route);
    }
}
