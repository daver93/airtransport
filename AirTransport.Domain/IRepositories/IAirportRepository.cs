﻿using AirTransport.Domain.Models;

namespace AirTransport.Domain.IRepositories
{
    public interface IAirportRepository
    {
        Task<IList<Airport?>> GetAirports();

        Task<Airport?> GetAirport(int id);

        Task<IList<Airport>> GetAirportsByGroupId(int airportGroupID);
    }
}
