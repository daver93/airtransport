﻿using AirTransport.Domain.Models;

namespace AirTransport.Domain.IRepositories
{
    public interface IGeographyLevel1Repository
    {
        Task<IList<GeographyLevel1>> GetCountries();

        Task<bool> CreateCountry(GeographyLevel1 country);

        Task<bool> DeleteCountry(int id);
    }
}
