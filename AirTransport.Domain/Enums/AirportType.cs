﻿namespace AirTransport.Domain.Enums
{
    public enum AirportType
    {
        ArrivalOnly,
        DepartureOnly,
        ArrivalAndDeparture
    }
}
