﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AirTransport.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class updatedWithAirportGroups : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AirportGroup",
                columns: table => new
                {
                    AirportGroupID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AirportGroup", x => x.AirportGroupID);
                });

            migrationBuilder.CreateTable(
                name: "AirportAirportGroup",
                columns: table => new
                {
                    AirportGroupsAirportGroupID = table.Column<int>(type: "int", nullable: false),
                    AirportsAirportID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AirportAirportGroup", x => new { x.AirportGroupsAirportGroupID, x.AirportsAirportID });
                    table.ForeignKey(
                        name: "FK_AirportAirportGroup_AirportGroup_AirportGroupsAirportGroupID",
                        column: x => x.AirportGroupsAirportGroupID,
                        principalTable: "AirportGroup",
                        principalColumn: "AirportGroupID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AirportAirportGroup_Airports_AirportsAirportID",
                        column: x => x.AirportsAirportID,
                        principalTable: "Airports",
                        principalColumn: "AirportID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AirportAirportGroup_AirportsAirportID",
                table: "AirportAirportGroup",
                column: "AirportsAirportID");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AirportAirportGroup");

            migrationBuilder.DropTable(
                name: "AirportGroup");
        }
    }
}
