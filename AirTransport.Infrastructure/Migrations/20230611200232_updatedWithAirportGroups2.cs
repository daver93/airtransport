﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AirTransport.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class updatedWithAirportGroups2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AirportAirportGroup_AirportGroup_AirportGroupsAirportGroupID",
                table: "AirportAirportGroup");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AirportGroup",
                table: "AirportGroup");

            migrationBuilder.RenameTable(
                name: "AirportGroup",
                newName: "AirportGroups");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AirportGroups",
                table: "AirportGroups",
                column: "AirportGroupID");

            migrationBuilder.AddForeignKey(
                name: "FK_AirportAirportGroup_AirportGroups_AirportGroupsAirportGroupID",
                table: "AirportAirportGroup",
                column: "AirportGroupsAirportGroupID",
                principalTable: "AirportGroups",
                principalColumn: "AirportGroupID",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AirportAirportGroup_AirportGroups_AirportGroupsAirportGroupID",
                table: "AirportAirportGroup");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AirportGroups",
                table: "AirportGroups");

            migrationBuilder.RenameTable(
                name: "AirportGroups",
                newName: "AirportGroup");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AirportGroup",
                table: "AirportGroup",
                column: "AirportGroupID");

            migrationBuilder.AddForeignKey(
                name: "FK_AirportAirportGroup_AirportGroup_AirportGroupsAirportGroupID",
                table: "AirportAirportGroup",
                column: "AirportGroupsAirportGroupID",
                principalTable: "AirportGroup",
                principalColumn: "AirportGroupID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
