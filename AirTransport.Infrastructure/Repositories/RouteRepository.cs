﻿using AirTransport.Domain.IRepositories;
using AirTransport.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace AirTransport.Infrastructure.Repositories
{
    public class RouteRepository : IRouteRepository
    {
        private readonly AirTransportDbContext _airTransportDbContext;

        public RouteRepository(AirTransportDbContext airTransportDbContext)
        {
            _airTransportDbContext = airTransportDbContext;
        }

        public async Task<IList<Route>> GetRoutes()
        {
            return await _airTransportDbContext.Routes.ToListAsync();
        }

        public async Task<Route?> GetRouteByDepartureAndArrival(int departureAirportID, int arrivalAirportID)
        {
            return await _airTransportDbContext.Routes.FirstOrDefaultAsync(r => r.DepartureAirportID == departureAirportID && r.ArrivalAirportID == arrivalAirportID);
        }

        public async Task<bool> CreateRoute(Route route)
        {
            _airTransportDbContext.Routes.Add(route);
            return await _airTransportDbContext.SaveChangesAsync() > 0;
        }
    }
}
