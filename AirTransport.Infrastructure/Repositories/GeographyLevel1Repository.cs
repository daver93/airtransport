﻿using AirTransport.Domain.IRepositories;
using AirTransport.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace AirTransport.Infrastructure.Repositories
{
    public class GeographyLevel1Repository : IGeographyLevel1Repository
    {
        private readonly AirTransportDbContext _airTransportDbContext;

        public GeographyLevel1Repository(AirTransportDbContext airTransportDbContext)
        {
            _airTransportDbContext = airTransportDbContext;
        }

        public async Task<IList<GeographyLevel1>> GetCountries()
        {
            return await _airTransportDbContext.GeographyLevel1s.ToListAsync();
        }

        public async Task<bool> CreateCountry(GeographyLevel1 country)
        {
            _airTransportDbContext.GeographyLevel1s.Add(country);
            return await _airTransportDbContext.SaveChangesAsync() > 0;
        }

        public async Task<bool> DeleteCountry(int id)
        {
            var country = await _airTransportDbContext.GeographyLevel1s.FirstOrDefaultAsync(g => g.GeographyLevel1ID == id);

            if (country != null) // the country with the given id exists
            {
                var usedByAirport = await _airTransportDbContext.Airports.FirstOrDefaultAsync(a => a.GeographyLevel1ID == country.GeographyLevel1ID);

                if (usedByAirport == null) // it is not being used, so we can safely delete the country
                {
                    _airTransportDbContext.GeographyLevel1s.Remove(country);
                    return await _airTransportDbContext.SaveChangesAsync() > 0;
                }
            }

            return false;
        }
    }
}