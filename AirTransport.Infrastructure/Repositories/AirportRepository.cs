﻿using AirTransport.Domain.IRepositories;
using AirTransport.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace AirTransport.Infrastructure.Repositories
{
    public class AirportRepository : IAirportRepository
    {
        private readonly AirTransportDbContext _airTransportDbContext;

        public AirportRepository(AirTransportDbContext airTransportDbContext)
        {
            _airTransportDbContext = airTransportDbContext;
        }

        public async Task<IList<Airport>> GetAirports()
        {
            return await _airTransportDbContext.Airports.ToListAsync();
        }

        public async Task<Airport> GetAirport(int id)
        {
            return await _airTransportDbContext.Airports.SingleOrDefaultAsync(a => a.AirportID == id);
        }

        public async Task<IList<Airport>> GetAirportsByGroupId(int airportGroupID)
        {
            return await _airTransportDbContext.Airports.Where(a => a.AirportGroups.Any(ag => ag.AirportGroupID == airportGroupID)).ToListAsync();
        }
    }

}
