﻿using Microsoft.EntityFrameworkCore;
using AirTransport.Domain.Models;

namespace AirTransport.Infrastructure
{
    public class AirTransportDbContext : DbContext
    {
        public AirTransportDbContext(DbContextOptions<AirTransportDbContext> options) : base(options) { }

        public DbSet<Airport> Airports { get; set; }
        public DbSet<GeographyLevel1> GeographyLevel1s { get; set; }
        public DbSet<Route> Routes { get; set; }
        public DbSet<AirportGroup> AirportGroups { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Route>()
                .HasOne(r => r.DepartureAirport)
                .WithMany()
                .HasForeignKey(r => r.DepartureAirportID)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Route>()
                .HasOne(r => r.ArrivalAirport)
                .WithMany()
                .HasForeignKey(r => r.ArrivalAirportID)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
