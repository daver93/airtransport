﻿using AirTransport.Domain.Enums;
using AirTransport.Domain.Models;
using AirTransport.Infrastructure;

namespace VendingMachineDemo.Data
{
    public static class SeedData
    {
        public static void InitializeDatabase(AirTransportDbContext context)
        {
            if (!context.GeographyLevel1s.Any())
            {
                context.GeographyLevel1s.AddRange(GetGeographyLevel1s());
                context.SaveChanges();
            }

            if (!context.Airports.Any())
            {
                context.Airports.AddRange(GetAirports());
                context.SaveChanges();
            }

            if (!context.Routes.Any())
            {
                context.Routes.AddRange(GetRoutes());
                context.SaveChanges();
            }

            if (!context.AirportGroups.Any())
            {
                context.AirportGroups.AddRange(GetAirportGroups(context));
                context.SaveChanges();
            }
        }

        private static List<Route> GetRoutes()
        {
            return new List<Route>
            {
                new Route { DepartureAirportID = 1, ArrivalAirportID = 2 }
            };
        }

        private static List<Airport> GetAirports()
        {
            return new List<Airport>
            {
                new Airport { IATACode = "LGW", GeographyLevel1ID = 1, Type = AirportType.ArrivalAndDeparture.ToString() },
                new Airport { IATACode = "PMI", GeographyLevel1ID = 2, Type = AirportType.ArrivalOnly.ToString() },
                new Airport { IATACode = "LAX", GeographyLevel1ID = 3, Type = AirportType.ArrivalOnly.ToString() },
                new Airport { IATACode = "ATH", GeographyLevel1ID = 4, Type = AirportType.ArrivalAndDeparture.ToString() },
                new Airport { IATACode = "SKG", GeographyLevel1ID = 4, Type = AirportType.ArrivalAndDeparture.ToString() },
                new Airport { IATACode = "LTN", GeographyLevel1ID = 1, Type = AirportType.ArrivalAndDeparture.ToString() }
            };
        }

        private static List<GeographyLevel1> GetGeographyLevel1s()
        {
            return new List<GeographyLevel1>
            {
                new GeographyLevel1 { Name = "United Kingdom"},
                new GeographyLevel1 { Name = "Spain"},
                new GeographyLevel1 { Name = "United States"},
                new GeographyLevel1 { Name = "Turkey"},
                new GeographyLevel1 { Name = "Greece"}
            };
        }

        private static List<AirportGroup> GetAirportGroups(AirTransportDbContext context)
        {
            var airportGroup1 = new AirportGroup { Name = "Group 1" };
            var airportGroup2 = new AirportGroup { Name = "Group 2" };
            var airportGroup3 = new AirportGroup { Name = "Group 3" };
            var airportGroup4 = new AirportGroup { Name = "Group 4" };

            // Retrieve the airports from the database
            var airport1 = context.Airports.FirstOrDefault(a => a.IATACode == "LGW");
            var airport2 = context.Airports.FirstOrDefault(a => a.IATACode == "PMI");
            var airport3 = context.Airports.FirstOrDefault(a => a.IATACode == "LAX");
            var airport4 = context.Airports.FirstOrDefault(a => a.IATACode == "ATH");
            var airport5 = context.Airports.FirstOrDefault(a => a.IATACode == "SKG");
            var airport6 = context.Airports.FirstOrDefault(a => a.IATACode == "LTN");

            if (airport1 != null)
            {
                airportGroup1.Airports.Add(airport1);
            }

            if (airport2 != null && airport3 != null)
            {
                airportGroup2.Airports.Add(airport2);
                airportGroup2.Airports.Add(airport3);
            }

            if (airport4 != null && airport5 != null)
            {
                airportGroup3.Airports.Add(airport4);
                airportGroup3.Airports.Add(airport5);
            }

            if (airport6 != null)
            {
                airportGroup4.Airports.Add(airport6);
            }

            return new List<AirportGroup>
            {
                airportGroup1,
                airportGroup2,
                airportGroup3,
                airportGroup4
            };
        }
    }
}
