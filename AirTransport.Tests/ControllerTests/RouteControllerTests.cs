﻿using AirTransport.Application.DTOs.Routes;
using AirTransport.Domain.Enums;
using AirTransport.Web.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace AirTransport.Tests.ControllerTests
{
    public class RouteControllerTests
    {
        private readonly Mock<IRouteService> _routeServiceMock;
        private readonly Mock<IAirportService> _airportServiceMock;
        private readonly RouteController _routeController;

        public RouteControllerTests()
        {
            _routeServiceMock = new Mock<IRouteService>();
            _airportServiceMock = new Mock<IAirportService>();
            _routeController = new RouteController(_routeServiceMock.Object, _airportServiceMock.Object);
        }

        [Fact]
        public async Task GetRoutes_ReturnsListOfRoutes()
        {
            // Arrange
            var routes = new List<Route>
            {
                new Route { RouteID = 1, DepartureAirportID = 1, ArrivalAirportID = 2 },
                new Route { RouteID = 2, DepartureAirportID = 2, ArrivalAirportID = 3 }
            };

            _routeServiceMock.Setup(service => service.GetRoutes()).ReturnsAsync(routes);

            // Act
            var result = await _routeController.GetRoutes();

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var response = Assert.IsType<GetRoutesResponse>(okResult.Value);
            Assert.Equal(routes.Count, response.Routes.Count);
        }

        [Fact]
        public async Task CreateRoute_ValidRequest_ReturnsOkResult()
        {
            // Arrange
            var request = new BaseRouteRequest
            {
                CreateRouteRequest = new CreateRouteRequest
                {
                    DepartureAirportID = 1,
                    ArrivalAirportID = 2
                }
            };

            _routeServiceMock.Setup(service => service.GetRouteByDepartureAndArrival(request.CreateRouteRequest.DepartureAirportID, request.CreateRouteRequest.ArrivalAirportID)).ReturnsAsync((Route)null);
            
            _airportServiceMock.Setup(service => service.GetAirportById(request.CreateRouteRequest.DepartureAirportID)).ReturnsAsync(new Airport { AirportID = 1, Type = AirportType.DepartureOnly.ToString() });
            _airportServiceMock.Setup(service => service.GetAirportById(request.CreateRouteRequest.ArrivalAirportID)).ReturnsAsync(new Airport { AirportID = 2, Type = AirportType.ArrivalOnly.ToString() });
            
            _routeServiceMock.Setup(service => service.CreateRoute(It.IsAny<Route>())).ReturnsAsync(true);

            // Act
            var result = await _routeController.CreateRoute(request);

            // Assert
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task CreateRoute_InvalidRequest_ReturnsBadRequest()
        {
            // Arrange
            var request = new BaseRouteRequest
            {
                CreateRouteRequest = new CreateRouteRequest
                {
                    DepartureAirportID = 0,
                    ArrivalAirportID = 0
                }
            };

            // Act
            var result = await _routeController.CreateRoute(request);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task CreateRouteWithGroups_ValidRequest_ReturnsOkResult()
        {
            // Arrange
            var request = new BaseRouteRequest
            {
                CreateGroupRouteRequest = new CreateGroupRouteRequest
                {
                    DepartureAirportGroupID = 1,
                    ArrivalAirportGroupID = 2
                }
            };

            var departureAirports = new List<Airport>
            {
                new Airport { AirportID = 1, Type = AirportType.DepartureOnly.ToString() },
                new Airport { AirportID = 2, Type = AirportType.DepartureOnly.ToString() }
            };
            
            var arrivalAirports = new List<Airport>
            {
                new Airport { AirportID = 3, Type = AirportType.ArrivalOnly.ToString() },
                new Airport { AirportID = 4, Type = AirportType.ArrivalOnly.ToString() }
            };
            
            _airportServiceMock.Setup(service => service.GetAirportsByGroupId(request.CreateGroupRouteRequest.DepartureAirportGroupID)).ReturnsAsync(departureAirports);
            _airportServiceMock.Setup(service => service.GetAirportsByGroupId(request.CreateGroupRouteRequest.ArrivalAirportGroupID)).ReturnsAsync(arrivalAirports);
            
            _routeServiceMock.Setup(service => service.GetRouteByDepartureAndArrival(It.IsAny<int>(), It.IsAny<int>())).ReturnsAsync((Route)null);
            _routeServiceMock.Setup(service => service.CreateRoute(It.IsAny<Route>())).ReturnsAsync(true);

            // Act
            var result = await _routeController.CreateRoute(request);

            // Assert
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task CreateRouteWithGroups_InvalidRequest_ReturnsBadRequest()
        {
            // Arrange
            var request = new BaseRouteRequest
            {
                CreateGroupRouteRequest = new CreateGroupRouteRequest
                {
                    DepartureAirportGroupID = 0,
                    ArrivalAirportGroupID = 0
                }
            };

            // Act
            var result = await _routeController.CreateRoute(request);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }
    }
}
