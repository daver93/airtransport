﻿using AirTransport.Application.DTOs.Countries;
using AirTransport.Web.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace AirTransport.Tests.ControllerTests
{
    public class CountryControllerTests
    {
        private readonly Mock<IGeographyLevel1Service> _countryServiceMock;
        private readonly CountryController _countryController;

        public CountryControllerTests()
        {
            _countryServiceMock = new Mock<IGeographyLevel1Service>();
            _countryController = new CountryController(_countryServiceMock.Object);
        }

        [Fact]
        public async Task GetCountries_ReturnsListOfCountries()
        {
            // Arrange
            var countries = new List<GeographyLevel1>
            {
                new GeographyLevel1 { GeographyLevel1ID = 1, Name = "United Kingdom" },
                new GeographyLevel1 { GeographyLevel1ID = 2, Name = "Spain" }
            };

            _countryServiceMock.Setup(service => service.GetCountries()).ReturnsAsync(countries);

            // Act
            var result = await _countryController.GetCountries();

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var response = Assert.IsType<GetCountriesResponse>(okResult.Value);
            
            Assert.Equal(countries.Count, response.Countries.Count);
        }

        [Fact]
        public async Task CreateCountry_ValidRequest_ReturnsOkResult()
        {
            // Arrange
            var request = new CreateCountryRequest { Name = "United States" };
            
            _countryServiceMock.Setup(service => service.CreateCountry(It.IsAny<GeographyLevel1>())).ReturnsAsync(true);

            // Act
            var result = await _countryController.CreateCountry(request);

            // Assert
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task CreateCountry_InvalidRequest_ReturnsBadRequest()
        {
            // Arrange
            var request = new CreateCountryRequest { Name = null };

            // Act
            var result = await _countryController.CreateCountry(request);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task DeleteCountry_ValidId_ReturnsOkResult()
        {
            // Arrange
            int countryId = 1;
            
            _countryServiceMock.Setup(service => service.DeleteCountry(countryId)).ReturnsAsync(true);

            // Act
            var result = await _countryController.DeleteCountry(countryId);

            // Assert
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task DeleteCountry_InvalidId_ReturnsBadRequest()
        {
            // Arrange
            int countryId = 1;
            
            _countryServiceMock.Setup(service => service.DeleteCountry(countryId)).ReturnsAsync(false);

            // Act
            var result = await _countryController.DeleteCountry(countryId);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }
    }
}
