﻿using AirTransport.Application.DTOs.Airports;
using AirTransport.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace AirTransport.Tests.ControllerTests
{
    public class AirportControllerTests
    {
        private readonly Mock<IAirportService> _airportServiceMock;
        private readonly AirportController _airportController;

        public AirportControllerTests()
        {
            _airportServiceMock = new Mock<IAirportService>();
            _airportController = new AirportController(_airportServiceMock.Object);
        }

        [Fact]
        public async Task GetAirports_ReturnsOkResultWithAirportList()
        {
            // Arrange
            var airports = new List<Airport>
            {
                new Airport { AirportID = 1, IATACode = "LGW", Type = "ArrivalAndDeparture", GeographyLevel1ID = 1 },
                new Airport { AirportID = 2, IATACode = "PMI", Type = "ArrivalOnly", GeographyLevel1ID = 2 }
            };

            _airportServiceMock.Setup(service => service.GetAirports()).ReturnsAsync(airports);

            // Act
            var actionResult = await _airportController.GetAirports();

            var okResult = actionResult.Result as OkObjectResult;
            var response = okResult.Value as GetAirportsResponse;

            // Assert
            Assert.NotNull(okResult);
            Assert.NotNull(response);
            Assert.Equal(2, response.Airports.Count);
        }

        [Fact]
        public async Task GetAirport_ExistingId_ReturnsOkResultWithAirportDTO()
        {
            // Arrange
            int airportId = 1;
            var airport = new Airport { AirportID = airportId, IATACode = "LGW", Type = "ArrivalAndDeparture", GeographyLevel1ID = 1 };
            
            _airportServiceMock.Setup(service => service.GetAirportById(airportId)).ReturnsAsync(airport);

            // Act
            var actionResult = await _airportController.GetAirport(airportId);
            var okResult = actionResult.Result as OkObjectResult;
            var response = okResult.Value as GetAirportResponse;

            // Assert
            Assert.NotNull(okResult);
            Assert.NotNull(response);
            Assert.Equal(airportId, response.Airport.Id);
        }

        [Fact]
        public async Task GetAirport_NonExistingId_ReturnsNotFoundResult()
        {
            // Arrange
            int airportId = 1;

            _airportServiceMock.Setup(service => service.GetAirportById(airportId)).ReturnsAsync((Airport)null);

            // Act
            var actionResult = await _airportController.GetAirport(airportId);
            var notFoundResult = actionResult.Result as NotFoundObjectResult;

            // Assert
            Assert.NotNull(notFoundResult);
            Assert.Equal("Airport not found.", notFoundResult.Value);
        }
    }
}
