﻿namespace AirTransport.Tests.ServiceTests
{
    public class GeographyLevel1ServiceTests
    {
        private readonly Mock<IGeographyLevel1Repository> _countryRepositoryMock;
        private readonly IGeographyLevel1Service _countryService;

        public GeographyLevel1ServiceTests()
        {
            _countryRepositoryMock = new Mock<IGeographyLevel1Repository>();
            _countryService = new GeographyLevel1Service(_countryRepositoryMock.Object);
        }

        [Fact]
        public async Task GetCountries_ReturnsListOfCountries()
        {
            // Arrange
            var countries = new List<GeographyLevel1>
            {
                new GeographyLevel1 { GeographyLevel1ID = 1, Name = "United Kingdom" },
                new GeographyLevel1 { GeographyLevel1ID = 2, Name = "Spain" }
            };

            _countryRepositoryMock.Setup(repo => repo.GetCountries()).ReturnsAsync(countries);

            // Act
            var result = await _countryService.GetCountries();

            // Assert
            Assert.Equal(countries, result);
        }

        [Fact]
        public async Task CreateCountry_ValidCountry_ReturnsTrue()
        {
            // Arrange
            var country = new GeographyLevel1 { Name = "United States" };
            
            _countryRepositoryMock.Setup(repo => repo.CreateCountry(country)).ReturnsAsync(true);

            // Act
            var result = await _countryService.CreateCountry(country);

            // Assert
            Assert.True(result);
        }

        [Fact]
        public async Task CreateCountry_InvalidCountry_ReturnsFalse()
        {
            // Arrange
            var country = new GeographyLevel1 { Name = null };
            
            _countryRepositoryMock.Setup(repo => repo.CreateCountry(country)).ReturnsAsync(false);

            // Act
            var result = await _countryService.CreateCountry(country);

            // Assert
            Assert.False(result);
        }

        [Fact]
        public async Task DeleteCountry_ExistingCountry_ReturnsTrue()
        {
            // Arrange
            int countryId = 1;
            
            _countryRepositoryMock.Setup(repo => repo.DeleteCountry(countryId)).ReturnsAsync(true);

            // Act
            var result = await _countryService.DeleteCountry(countryId);

            // Assert
            Assert.True(result);
        }

        [Fact]
        public async Task DeleteCountry_NonExistingCountry_ReturnsFalse()
        {
            // Arrange
            int countryId = 1;
            
            _countryRepositoryMock.Setup(repo => repo.DeleteCountry(countryId)).ReturnsAsync(false);

            // Act
            var result = await _countryService.DeleteCountry(countryId);

            // Assert
            Assert.False(result);
        }
    }
}
