namespace AirTransport.Tests.ServiceTests
{
    public class AirportServiceTests
    {
        private readonly Mock<IAirportRepository> _airportRepositoryMock;
        private readonly IAirportService _airportService;

        public AirportServiceTests()
        {
            _airportRepositoryMock = new Mock<IAirportRepository>();
            _airportService = new AirportService(_airportRepositoryMock.Object);
        }

        [Fact]
        public async Task GetAirports_ReturnsListOfAirports()
        {
            // Arrange
            var airports = new List<Airport> { new Airport(), new Airport() };

            _airportRepositoryMock.Setup(repo => repo.GetAirports()).ReturnsAsync(airports);

            // Act
            var result = await _airportService.GetAirports();

            // Assert
            Assert.Equal(airports.Count, result.Count);
        }

        [Fact]
        public async Task GetAirportById_ExistingId_ReturnsAirport()
        {
            // Arrange
            int airportId = 1;
            var airport = new Airport { AirportID = airportId };

            _airportRepositoryMock.Setup(repo => repo.GetAirport(airportId)).ReturnsAsync(airport);

            // Act
            var result = await _airportService.GetAirportById(airportId);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(airportId, result.AirportID);
        }

        [Fact]
        public async Task GetAirportsByGroupId_ExistingGroupId_ReturnsListOfAirports()
        {
            // Arrange
            int groupId = 1;
            var airports = new List<Airport> { new Airport(), new Airport() };

            _airportRepositoryMock.Setup(repo => repo.GetAirportsByGroupId(groupId)).ReturnsAsync(airports);

            // Act
            var result = await _airportService.GetAirportsByGroupId(groupId);

            // Assert
            Assert.Equal(airports.Count, result.Count);
        }
    }
}