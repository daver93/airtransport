﻿namespace AirTransport.Tests.ServiceTests
{
    public class RouteServiceTests
    {
        private readonly Mock<IRouteRepository> _routeRepositoryMock;
        private readonly IRouteService _routeService;

        public RouteServiceTests()
        {
            _routeRepositoryMock = new Mock<IRouteRepository>();
            _routeService = new RouteService(_routeRepositoryMock.Object);
        }

        [Fact]
        public async Task GetRoutes_ReturnsListOfRoutes()
        {
            // Arrange
            var routes = new List<Route>
            {
                new Route { RouteID = 1, DepartureAirportID = 1, ArrivalAirportID = 2 },
                new Route { RouteID = 2, DepartureAirportID = 2, ArrivalAirportID = 3 }
            };

            _routeRepositoryMock.Setup(repo => repo.GetRoutes()).ReturnsAsync(routes);

            // Act
            var result = await _routeService.GetRoutes();

            // Assert
            Assert.Equal(routes, result);
        }

        [Fact]
        public async Task GetRouteByDepartureAndArrival_ExistingRoute_ReturnsRoute()
        {
            // Arrange
            int departureAirportID = 1;
            int arrivalAirportID = 2;
            var route = new Route { RouteID = 1, DepartureAirportID = departureAirportID, ArrivalAirportID = arrivalAirportID };
            
            _routeRepositoryMock.Setup(repo => repo.GetRouteByDepartureAndArrival(departureAirportID, arrivalAirportID)).ReturnsAsync(route);

            // Act
            var result = await _routeService.GetRouteByDepartureAndArrival(departureAirportID, arrivalAirportID);

            // Assert
            Assert.Equal(route, result);
        }

        [Fact]
        public async Task GetRouteByDepartureAndArrival_NonExistingRoute_ReturnsNull()
        {
            // Arrange
            int departureAirportID = 1;
            int arrivalAirportID = 2;

            _routeRepositoryMock.Setup(repo => repo.GetRouteByDepartureAndArrival(departureAirportID, arrivalAirportID)).ReturnsAsync((Route)null);

            // Act
            var result = await _routeService.GetRouteByDepartureAndArrival(departureAirportID, arrivalAirportID);

            // Assert
            Assert.Null(result);
        }

        [Fact]
        public async Task CreateRoute_ValidRoute_ReturnsTrue()
        {
            // Arrange
            var route = new Route { DepartureAirportID = 1, ArrivalAirportID = 2 };

            _routeRepositoryMock.Setup(repo => repo.CreateRoute(route)).ReturnsAsync(true);

            // Act
            var result = await _routeService.CreateRoute(route);

            // Assert
            Assert.True(result);
        }

        [Fact]
        public async Task CreateRoute_InvalidRoute_ReturnsFalse()
        {
            // Arrange
            var route = new Route { DepartureAirportID = 1, ArrivalAirportID = 1 };
            _routeRepositoryMock.Setup(repo => repo.CreateRoute(route)).ReturnsAsync(false);

            // Act
            var result = await _routeService.CreateRoute(route);

            // Assert
            Assert.False(result);
        }
    }
}
