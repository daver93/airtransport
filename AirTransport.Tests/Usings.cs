global using Xunit;
global using Moq;
global using AirTransport.Domain.IRepositories;
global using AirTransport.Domain.IServices;
global using AirTransport.Domain.Models;
global using AirTransport.Application.Services;